Name:            rng-tools
Version:         6.16
Release:         2
Summary:         Random number generator daemon
License:         GPLv2+
URL:             https://github.com/nhorman/rng-tools
Source0:         https://github.com/nhorman/rng-tools/archive/v%{version}.tar.gz
Source1:         rngd.service

#Dependency
BuildRequires:   gcc make gettext systemd autoconf automake
BuildRequires:   libgcrypt-devel libcurl-devel libxml2-devel openssl-devel
BuildRequires:   libp11-devel jitterentropy-library-devel jansson-devel libcap-devel
Requires:        libgcrypt libsysfs openssl libxml2 libcurl jitterentropy-library openssl-pkcs11
Requires:        opensc
%{?systemd_requires}

%description
Rng-tools is a random number generator daemon.It monitors a set of entropy sources,
and supplies entropy from them to the system kernel's /dev/random machinery.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
./autogen.sh
%configure --without-rtlsdr
%make_build

%install
%make_install

install -D -t $RPM_BUILD_ROOT%{_unitdir} -m 0644 %{SOURCE1}

%check
export RNGD_JITTER_TIMEOUT=10
%make_build check

%pre

%post
%systemd_post rngd.service

%preun
%systemd_preun rngd.service

%postun
%systemd_postun_with_restart rngd.service

%files
%defattr(-,root,root)
%license COPYING
%doc AUTHORS NEWS README
%{_bindir}/rngtest
%{_bindir}/randstat
%{_sbindir}/rngd
%attr(0644,root,root) %{_unitdir}/rngd.service

%files help
%{_mandir}/man1/rngtest.1.*
%{_mandir}/man8/rngd.8.*

%changelog
* Fri Mar 31 2023 fuanan <fuanan3@h-partners.com> - 6.16-2
- enable make check

* Sat Jan 28 2023 fuanan <fuanan3@h-partners.com> - 6.16-1
- update version to 6.16

* Fri Sep 2 2022 panxiaohe <panxh.life@foxmail.com> - 6.14-4
- fix changelog to make it in descending chronological order

* Thu Jul 28 2022 zhangruifang <zhangruifang1@h-partners.com> - 6.14-3
- update release

* Tue Jan 18 2022 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 6.14-2
- Add requires:opensc,prevent service startup failure logs

* Wed Dec 29 2021 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 6.14-1
- update version to 6.14

* Sat Dec 19 2020 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 6.5-3
- fix rngd.service coredump

* Thu Dec 10 2020 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 6.5-2
- fix rngd.service coredump

* Wed Sep 2 2020 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 6.5-1
- since 6.6, jitterentropy-library is independed from rng-tools,
  no any entropy source will lead to rng-tools service fail.

* Thu Jul 30 2020 zhangxingliang <zhangxingliang3@huawei.com> - 6.10-1
- update to 6.10

* Sat Feb 29 2020 openEuler Buildteam <buildteam@openeuler.org> - 6.3.1-4
- add jitterentropy support

* Wed Feb 19 2020 wanjiankang <wanjiankang@huawei.com> - 6.3.1-3
- Package init
